# MyDrive Challenge

# Challenge

    MyDrive Solutions sales employees travel very often, and they will need an application to assist them with the currency exchange rate for each country they visit. The task is to create an iOS application to assist them with currency conversions. 

# Solution

- [x] The app should get the latest exchange rates from this endpoint: https://raw.githubusercontent.com/mydrive/code-tests/master/iOS-currency-exchange-rates/rates.json
- [x] The user should be able to select which currency to convert from and to.
- [x] Both 'from' and 'to' selectors should contain ALL currencies (there's enough information to convert from any to all).
- [x] The app should calculate the exchange rate after the user has selected the "from" and "to" currencies.
- [x] Write the unit tests wherever you see fit
- [x] Code should be uploaded to an online code repository that MyDrive staff can be given access to. e.g. Github, bitbucket or equivalent.
- [x] Evaluate your solution, list its strengths and weaknesses.
    - Strenghts
        - Small
        - Readable
        - Can be use in production
        - To add a new currency and rate just have to update JSON
    - Weaknesses
        - Should use real database, like CoreData or Realm
        - The app is not beautiful, needs to improve the UI
- [x] Please let us know how long this task took you.
    - The first part it took me 3 hours, the bonus part took me 2 hours

- [x] Document the development process for this app (Decisions, libraries, etc).
    - Didn't use Cocoapods for this project, it should if uses a real database like Realm
    - Everything is native, didn't use any third-party libraries
    - Prefer to use URLSession over the Alamofire because Swift 3 new URLSession was really nice if you work with flatMap(),
        so for this project there's no need to use a huge and complex library like Alamofire
    - Had to use class instead of struct in the "Entities" because to archive and unarchive in UserDefaults, since I have to implement
        NSCoding protocol
    - For this project I prefered to work with Storyboard because had only one View

- [] Demonstrate good practices in source code management.
    - Unfortunately I just upload to Bitbucket when I finished, so it will lack from commits ):
- [x] Offline capabilities.
- [ ] Run in a real device
    - I didn't have a valid certificate to use in this Challenge, so unfortunatelly can't run in a real device.

# Aditional Questions

- How do you keep up with the latest iOS development practices?
    - Always watch WWDC videos
    - Follow great developers at Twitter and their blog, like Florian Kluger, Jon Reid, Felix Krause, Chris Eidhof
    - Check the iOS Developer International (and BR) community in Slack
    - Follow importants blogs, like objc.iOS

- List some of your favourite iOS libraries including a brief description of each.
    - Realm
        - Really nice library for database, much better than CoreData because it's easy to initialize, and eliminates a lot of boilerplate code
    - PINRemoteImage
        - Pinterest's library is so cool, they really put an effort to it to create a greate library, and this one is really nice.
            It's so fast, so easy to use.
    - Pop
        - Facebook's pop makes animation so easy.

- What are the top 5 tools that you could not normally live without?
    - Visual Studio Code 
        - Besides XCode, I always have a VS Code opened in my computer. It's great to update quickly fast files, like Fastfile, Podfile, etc...
    - Sourcetree
        - Even I can use GIT in Terminal, a visual GIT improves a lot the production. When you need to see what's really going on when you merge, rebase,
            and keeps the stash much more nice to use.
    - BetterSnapTool
        - So much love <3 Shortcuts to split views, fullscreen view, and also to open apps
    - Zeplin
        - Collaboration app for UI designers and developers
    - SwaggerHub
        - Collaboration API blueprint for backend developers and frontend developers

    - BONUS: Slack <3
