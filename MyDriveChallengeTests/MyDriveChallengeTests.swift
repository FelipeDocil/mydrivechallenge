//
//  MyDriveChallengeTests.swift
//  MyDriveChallengeTests
//
//  Created by Felipe Docil on 11/2/16.
//  Copyright © 2016 Felipe Docil. All rights reserved.
//

import XCTest
@testable import MyDriveChallenge

class MyDriveChallengeTests: XCTestCase {
    
    let mockEUR = Currency(code: "EUR")
    let mockUSD = Currency(code: "USD")
    let mockJPY = Currency(code: "JPY")
    let mockGBP = Currency(code: "GBP")
    
    override func setUp() {
        ConversionManager.downloadConversions()
        sleep(3) // Need to wait the download to finish
    }
    
    /// 10 EUR to USD
    /// Total: 10.9833
    func test10EURToUSD() {
        let value = ConversionManager.exchange(value: 10, from: mockEUR, to: mockUSD)
        
        XCTAssertEqual(Double(round(100*value)/100), 10.98)
    }
    
    /// 1 USD to EUR
    /// Total: 0.91
    func testUSDToEUR() {
        let value = ConversionManager.exchange(value: 1, from: mockUSD, to: mockEUR)
        
        XCTAssertEqual(Double(round(100*value)/100), 0.91)
    }
    
    /// 2 EUR to JPY
    /// Total: 271.9
    func test2EURToJPY() {
        let value = ConversionManager.exchange(value: 2, from: mockEUR, to: mockJPY)
        
        XCTAssertEqual(Double(round(100*value)/100), 271.9)
    }
    
    /// 1 USD to GBP
    /// Total: 0.65
    func testUSDToGBP() {
        let value = ConversionManager.exchange(value: 1, from: mockUSD, to: mockGBP)
        
        XCTAssertEqual(Double(round(100*value)/100), 0.65)
    }
    
    /// 1 EUR to GBP
    /// Total: 0.72
    func testEURToGBP() {
        let value = ConversionManager.exchange(value: 1, from: mockEUR, to: mockGBP)
        
        XCTAssertEqual(Double(round(100*value)/100), 0.72)
    }
    
    /// 1 EUR to EUR
    /// Total: 1
    func testEURToEUR() {
        let value = ConversionManager.exchange(value: 1, from: mockEUR, to: mockEUR)
        
        XCTAssertEqual(Double(round(100*value)/100), 1)
    }
}
