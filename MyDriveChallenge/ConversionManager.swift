//
//  ConversionManager.swift
//  MyDriveChallenge
//
//  Created by Felipe Docil on 11/2/16.
//  Copyright © 2016 Felipe Docil. All rights reserved.
//

import Foundation

class ConversionManager {
    static private var items: [Conversion]?
    
    static private func cachedItems() {
        guard let cachedItems = UserDefaults.standard.data(forKey: "kCachedItems") else { itemsFromFile(); return }
        
        items = NSKeyedUnarchiver.unarchiveObject(with: cachedItems) as! [Conversion]?
    }
    
    static private func itemsFromFile() {
        if let path = Bundle.main.path(forResource: "rates", ofType: "json") {
            if let jsonData = try? NSData(contentsOfFile: path, options: .mappedIfSafe) {
                let json = try? JSONSerialization.jsonObject(with: jsonData as Data, options: .allowFragments)
                if let json = json as? [String: Any], let conversions = json["conversions"] as? [[String: Any]] {
                    items = conversions.flatMap(Conversion.init)
                    
                    let cachedItems = NSKeyedArchiver.archivedData(withRootObject: items as Any)
                    UserDefaults.standard.set(cachedItems, forKey: "kCachedItems")
                    
                    let cachedCurrency = NSKeyedArchiver.archivedData(withRootObject: getAllCurrenciesCode() as Any)
                    UserDefaults.standard.set(cachedCurrency, forKey: "kCachedCurrency")
                    UserDefaults.standard.synchronize()
                }
            }
        }
    }
    
    static func downloadConversions() {
        
        let urlString = "https://raw.githubusercontent.com/mydrive/code-tests/master/iOS-currency-exchange-rates/rates.json"
        let url = URL(string: urlString)
        
        let session = URLSession.shared
        
        session.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                return
            }
            
            guard let serverResponse = response as? HTTPURLResponse else {
                return
            }
            
            switch serverResponse.statusCode {
            case 200..<300:
                
                let json = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = json as? [String: Any], let conversions = json["conversions"] as? [[String: Any]] {
                    items = conversions.flatMap(Conversion.init)
                    
                    let cachedItems = NSKeyedArchiver.archivedData(withRootObject: items as Any)
                    UserDefaults.standard.set(cachedItems, forKey: "kCachedItems")
                    
                    let cachedCurrency = NSKeyedArchiver.archivedData(withRootObject: getAllCurrenciesCode() as Any)
                    UserDefaults.standard.set(cachedCurrency, forKey: "kCachedCurrency")
                    UserDefaults.standard.synchronize()
                }
                
            default:
                break;
            }
            
            }.resume()
    }
    
    static private func getAllCurrenciesCode() -> [String]? {
        guard let items = items else { cachedItems(); return nil }
        
        var allCurrencies: [String] = []
        
        for item in items {
            allCurrencies.append(item.from.code)
            allCurrencies.append(item.to.code)
        }
        
        let set = NSSet(array: allCurrencies)
        allCurrencies = set.allObjects as! [String]
        
        return allCurrencies.sorted()
    }
    
    static private func conversion(from: Currency, to: Currency) -> Conversion? {
        guard let items = items, items.count > 0 else { cachedItems(); return nil }
        
        let filtered = items.filter { $0.from.code == from.code && $0.to.code == to.code }
        return filtered.first
    }
    
    static func exchange(value: Double, from: Currency, to: Currency) -> Double {
        
        if from == to {
            return value
        }
        
        if let rate = conversion(from: from, to: to)?.rate {
            return value * rate
        } else {
            if let rate = conversion(from: to, to: from)?.rate {
                return value / rate
            }
        }
        
        let usd = Currency(code: "USD")
        let usdValue = exchange(value: value, from: from, to: usd)
        
        return exchange(value: usdValue, from: usd, to: to)
    }
    
    static func allCurrenciesCode() -> [String]? {
        guard let cachedCurrencies = UserDefaults.standard.data(forKey: "kCachedCurrency") else { cachedItems(); return nil }
        return NSKeyedUnarchiver.unarchiveObject(with: cachedCurrencies) as! [String]?
    }
}
