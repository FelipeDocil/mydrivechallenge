//
//  Conversion.swift
//  MyDriveChallenge
//
//  Created by Felipe Docil on 11/2/16.
//  Copyright © 2016 Felipe Docil. All rights reserved.
//

import Foundation

class Conversion: NSObject, NSCoding {
    let from: Currency
    let to: Currency
    let rate: Double
    
    init(from: Currency, to: Currency, rate: Double) {
        self.from = from
        self.to = to
        self.rate = rate
    }
    
    init?(json: [String: Any]) {
        guard
            let from = json["from"] as? String,
            let to = json["to"] as? String,
            let rate = json["rate"] as? Double
            else { return nil }
        
        self.from = Currency(code: from)
        self.to = Currency(code: to)
        self.rate = rate
    }
    
    required init?(coder aDecoder: NSCoder) {
        guard let from = aDecoder.decodeObject(forKey: "from") as? Currency else { return nil }
        guard let to = aDecoder.decodeObject(forKey: "to") as? Currency else { return nil }
        guard let rate = aDecoder.decodeObject(forKey: "rate") as? String else { return nil }
        
        self.from = from
        self.to = to
        self.rate = Double(rate)!
        
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(from, forKey: "from")
        aCoder.encode(to, forKey: "to")
        aCoder.encode("\(rate)", forKey: "rate")
    }
}
