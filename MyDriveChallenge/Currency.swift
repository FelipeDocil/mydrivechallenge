//
//  Currency.swift
//  MyDriveChallenge
//
//  Created by Felipe Docil on 11/2/16.
//  Copyright © 2016 Felipe Docil. All rights reserved.
//

import Foundation

class Currency: NSObject, NSCoding {
    
    let code: String
    
    init(code: String) {
        self.code = code
    }
    
    required init?(coder aDecoder: NSCoder) {
        guard let code = aDecoder.decodeObject(forKey: "code") as? String else {  return nil }
        
        self.code = code
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(code, forKey: "code")
    }
}
