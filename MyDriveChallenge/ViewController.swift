//
//  ViewController.swift
//  MyDriveChallenge
//
//  Created by Felipe Docil on 11/2/16.
//  Copyright © 2016 Felipe Docil. All rights reserved.
//

import UIKit

enum ButtonType: Int {
    case from
    case to
}

class ViewController: UIViewController {

    @IBOutlet weak var fromButton: UIButton!
    @IBOutlet weak var toButton: UIButton!
    @IBOutlet weak var fromCurrencySymbolLabel: UILabel!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toCurrencySymbolLabel: UILabel!
    @IBOutlet weak var resultTextField: UITextField!
    
    var from: Currency?
    var to: Currency?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ConversionManager.downloadConversions()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }


    // MARK: Buttons Action
    @IBAction func didTapFromButton(_ sender: Any) {
        button { code in
            self.fromButton.setTitle(code, for: .normal)
            self.from = Currency(code: code)
            self.fromCurrencySymbolLabel.text = self.symbol(from: code)
        }
    }
    
    
    @IBAction func didTapToButton(_ sender: Any) {
        button { code in
            self.toButton.setTitle(code, for: .normal)
            self.to = Currency(code: code)
            self.toCurrencySymbolLabel.text = self.symbol(from: code)
        }
    }
    
    @IBAction func didTapCalculateButton(_ sender: Any) {
        calculate()
    }
    
    
    // MARK: Helpers
    func button(handler: @escaping ((String) -> Void)) {
        guard let allCurrencies = ConversionManager.allCurrenciesCode() else { return }
        
        let actionSheet = UIAlertController(title: "Choose a currency", message: "", preferredStyle: .actionSheet)
        
        for code in allCurrencies {
            actionSheet.addAction(UIAlertAction(title: code, style: .default, handler: { action in
                handler(code)
                self.calculate()
            }))
        }
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func symbol(from code: String) -> String? {
        let locale = NSLocale(localeIdentifier: code)
        return locale.displayName(forKey: NSLocale.Key.currencySymbol, value: code)
    }
    
    func calculate() {
        fromTextField.resignFirstResponder()
        
        guard
            let text = fromTextField.text,
            let value = Double(text),
            let from = from,
            let to = to
            else { return }
        
        let result = ConversionManager.exchange(value: value, from: from, to: to)
        resultTextField.text = "\(Double(round(100*result)/100))"

    }
}

